SoapBox iOS Project

Requirements

-Use Giphy’s API to search for tags and display a list of GIFs.
-Handling of multi-paged items.
-Tapping on a GIF should bring that item into focus and display more information.
-Ability to rate the GIFs and sort by highest rank.
-Use Storyboards.

Dependencies

-Alamofire
-Cosmos
-SwiftyGif
-SwiftyJSON

Dependency Manager

-Cocoa Pods
-Please use .xcworkspace file


