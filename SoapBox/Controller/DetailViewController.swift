//
//  DetailViewController.swift
//  SoapBox
//
//  Created by Hyewook Song on 2018-05-17.
//  Copyright © 2018 Hailey Song. All rights reserved.
//

import UIKit
import SwiftyGif
import Cosmos

protocol RatingDelegate: class {
    func ratingChanged()
}

class DetailViewController: UIViewController {
    
    @IBOutlet weak var gifView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingStar: CosmosView!
    
    private let coredataHandler = CoreDataHandler()
    weak var delegate: RatingDelegate?
    
    var gif: GIFModel? {
        didSet {
            configureView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    func configureView() {

        if var gifData = gif {
            
            if let iv = gifView {
                if let url = URL(string: gifData.image) {
                    iv.setGifFromURL(url)
                }
            }
            if let title = titleLabel {
                title.sizeToFit()
                title.text = gifData.title
            }
            if let rate = ratingStar {
                rate.rating = coredataHandler.getRating(id: gifData.id)
                rate.didFinishTouchingCosmos = { rating in
                    gifData.rating = rating
                    self.coredataHandler.checkSaveUpdate(gif: gifData)
                    self.delegate?.ratingChanged()
                }
            }
        }
    }
}
