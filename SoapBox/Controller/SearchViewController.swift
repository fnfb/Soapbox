//
//  SearchViewController.swift
//  SoapBox
//
//  Created by Hyewook Song on 2018-05-17.
//  Copyright © 2018 Hailey Song. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBAction func sortBtn(_ sender: Any) {
        dataArray.removeAll()
        coredataHandler.getAllGif()
        isSortClicked = true
        view.endEditing(true)
    }
    
    private let apiHandler = APIHandler()
    private let coredataHandler = CoreDataHandler()
    public var dataArray:[GIFModel] = []
    let cellId = "gifCell"
    var isSortClicked: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        apiHandler.delegate = self
        coredataHandler.delegate = self
        
        // CollectionView Layout
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 20
        let cellSize = (view.frame.width - 60)/2
        layout.itemSize = CGSize(width: cellSize, height: cellSize)
        collectionView.collectionViewLayout = layout
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    
    
    
    
    // MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath as IndexPath) as! GIFCell
        cell.configure(gif: dataArray[indexPath.item])
        
        return cell
    }
    
    // Reached the end of collection view
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height

        if maximumOffset - currentOffset <= 10.0 {
            if !apiHandler.endPage {
                apiHandler.requestNextPage()
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showGifDetail" {
            let dc = segue.destination as! DetailViewController
            let selectedCell = sender as! GIFCell
            let indexPath = collectionView.indexPath(for: selectedCell)
            dc.delegate = self
            dc.gif = dataArray[indexPath!.row]
        }
    }
    

    
    
    
    
    
    // MARK: SearchBar
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dataArray.removeAll()
        apiHandler.requestGif(searchText: searchBar.text!, offset: "0")
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        isSortClicked = false
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
    }
}

extension SearchViewController: GIFDelegate {
    
    func didReceiveData(gifs: [GIFModel]) {
        dataArray.append(contentsOf: gifs)
        collectionView.reloadData()
    }
}

extension SearchViewController: SortDelegate {
    
    func didGetData(gifs: [GIFModel]) {
        dataArray = gifs
        dataArray.sort { $0.rating > $1.rating }
        collectionView.reloadData()
    }
}

extension SearchViewController: RatingDelegate {
    
    func ratingChanged() {
        if isSortClicked {
            dataArray.removeAll()
            coredataHandler.getAllGif()
        }
    }
}
