//
//  APIHandler.swift
//  SoapBox
//
//  Created by Hyewook Song on 2018-05-17.
//  Copyright © 2018 Hailey Song. All rights reserved.
//

import SwiftyJSON
import Alamofire

protocol GIFDelegate: class {
    func didReceiveData(gifs: [GIFModel])
}

class APIHandler {
    
    weak var delegate:GIFDelegate?
    
    private var totalCount:Int
    private var currentOffset: Int
    private var countLimit = 25
    public var endPage: Bool
    private var currentText:String
    
    init(){
        totalCount = 0
        currentOffset = 0
        endPage = true
        currentText = ""
    }
    
    func resetSearch(){
        totalCount = 0
        currentOffset = 0
        endPage = true
        currentText = ""
    }
    
    func requestGif(searchText: String, offset: String) {
        
        // Check searchText is not empty & replace spaces
        if (!searchText.isEmpty) {
            self.resetSearch()
            
            let modifiedText = searchText.replacingOccurrences(of: " ", with: "+")
            currentText = modifiedText
            
            if let url = URL(string: "https://api.giphy.com/v1/gifs/search?q="+modifiedText+"&offset="+offset+"&api_key=F5gGxhCPhReuwFgVr5c510GhNJuebgMD") {
                
                Alamofire.request(url).validate().responseJSON { response in
                    switch response.result {
                        
                    case .success:
                        if let value = response.result.value {
                            let json = JSON(value)
                            if (!json["error"].exists()) {
                                self.parseJSON(json: json)
                            } else {
                                print(json["error"])
                            }
                        }
                    case .failure(let error):
                        print(error)
                    }
                }
            } else {
                print("FOUND NIL URL")
            }
        }
    }
    
    func parseJSON(json: JSON) {

        currentOffset = json["pagination"]["offset"].intValue
        totalCount = json["pagination"]["total_count"].intValue

        if (currentOffset < totalCount) {
            endPage = false
        } else {
            endPage = true
        }
        
        let gifs = json["data"].arrayValue
        var tempArray: [GIFModel] = []
        
        for item in gifs {
            if let gif = GIFModel(id: item["id"].stringValue,
                                     url: item["images"]["fixed_width"]["url"].stringValue,
                                     title: item["title"].stringValue,
                                     rating: 0.0) as GIFModel? {
                tempArray.append(gif)
            }
        }
        delegate?.didReceiveData(gifs: tempArray)
    }
    
    func requestNextPage() {
        requestGif(searchText: currentText, offset: String(currentOffset+countLimit))
    }
}
