//
//  CoreDataHandler.swift
//  SoapBox
//
//  Created by Hyewook Song on 2018-05-18.
//  Copyright © 2018 Hailey Song. All rights reserved.
//

import UIKit
import CoreData

protocol SortDelegate: class {
    func didGetData(gifs: [GIFModel])
}

class CoreDataHandler {
    
    weak var delegate:SortDelegate?
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    func getAllGif() {
        
        var resultArray: [GIFModel] = []
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Gif")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            if results.count > 0 {
                for result in results as! [Gif] {
                    if let gif = GIFModel(id: result.id!,
                                          url: result.imageUrl!,
                                          title: result.title!,
                                          rating: result.rating) as GIFModel? {
                        resultArray.append(gif)
                    }
                }
                delegate?.didGetData(gifs: resultArray)
                
            } else {
                print("NO VALUE IN CORE DATA")
            }
        } catch {
            print("COULDNT FETCH CORE DATA RESULTS")
        }
    }
    
    // If the given gif exists in Core Data, create entity, else update rating
    func checkSaveUpdate(gif: GIFModel) {
        
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Gif")
        request.returnsObjectsAsFaults = false
        request.predicate = NSPredicate(format: "id == %@", gif.id)
        
        do {
            let results = try context.fetch(request) as! [Gif]
            if results.count > 0 {
                results[0].setValue(gif.rating, forKey: "rating")
                
            } else {
                let newGif = Gif(context: context)
                newGif.setValue(gif.id, forKey: "id")
                newGif.setValue(gif.title, forKey: "title")
                newGif.setValue(gif.rating, forKey: "rating")
                newGif.setValue(gif.image, forKey: "imageUrl")
            }
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        } catch {
            print("COULDNT FETCH CORE DATA RESULTS")
        }
    }
    
    func getRating(id: String) -> Double {
        
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Gif")
        request.returnsObjectsAsFaults = false
        request.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let results = try context.fetch(request) as! [Gif]
            if results.count > 0 {
                return results[0].value(forKey: "rating") as! Double
            }
        } catch {
            print("COULDNT FETCH CORE DATA RESULTS")
        }
        
        return 0.0
    }
}
