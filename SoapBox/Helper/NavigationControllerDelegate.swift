//
//  NavigationControllerDelegate.swift
//  SoapBox
//
//  Created by Hyewook Song on 2018-05-19.
//  Copyright © 2018 Hailey Song. All rights reserved.
//

import UIKit

class NavigationControllerDelegate: NSObject, UINavigationControllerDelegate {

    func navigationController(_ navigationController: UINavigationController,
        animationControllerFor operation: UINavigationControllerOperation,
        from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return CustomAnimator()
    }
}
