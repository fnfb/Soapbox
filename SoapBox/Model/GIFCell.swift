//
//  GIFCell.swift
//  SoapBox
//
//  Created by Hyewook Song on 2018-05-17.
//  Copyright © 2018 Hailey Song. All rights reserved.
//

import UIKit
import SwiftyGif

class GIFCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    func configure(gif: GIFModel) {
        imageView.image = nil
        if let url = URL(string: gif.image) {
            imageView.setGifFromURL(url)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        //imageView?.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        //imageView.image = nil
    }
}
