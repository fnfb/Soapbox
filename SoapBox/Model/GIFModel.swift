//
//  GIFModel.swift
//  SoapBox
//
//  Created by Hyewook Song on 2018-05-17.
//  Copyright © 2018 Hailey Song. All rights reserved.
//

import UIKit

struct GIFModel {
    
    let id:String
    let image:String
    let title:String
    var rating:Double
    
    init(id: String, url: String, title: String, rating: Double){
        self.id = id
        self.image = url
        self.title = title
        self.rating = rating
    }
}
