//
//  DetailTest.swift
//  SoapBoxTests
//
//  Created by Hyewook Song on 2018-05-18.
//  Copyright © 2018 Hailey Song. All rights reserved.
//

import XCTest
import SwiftyGif
@testable import SoapBox

class DetailTest: XCTestCase {
    
    var detail: DetailViewController!
    var systemUnderTest: SearchViewController!
    var dataSource: GIFModel = GIFModel(id: "FG6EQNhs3s7ug",
                                        url: "https://media2.giphy.com/media/FG6EQNhs3s7ug/200w.gif",
                                        title: "fat cat GIF",
                                        rating: 0.0)
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        detail = storyboard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        _ = detail.view
        systemUnderTest = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        _ = systemUnderTest.view
    }
    
    func test_Nil() {
        XCTAssertEqual(detail.titleLabel.text, "")
        XCTAssertNil(detail.gifView.image)
        XCTAssertEqual(detail.ratingStar.rating, 0.0)
    }
    
    func hasSegueWithIdentifier(id: String) -> Bool {
        let segues = systemUnderTest.value(forKey: "storyboardSegueTemplates") as? [NSObject]
        if let filtered = segues?.filter({ $0.value(forKey: "identifier") as? String == id }) {
            if filtered.count > 0 {
                return true
            }
        }
        return false
    }
    
    func test_HasSegue() {
        let targetIdentifier = "showGifDetail"
        XCTAssertTrue(hasSegueWithIdentifier(id: targetIdentifier))
    }
    
    func test_UIInView() {
        detail.gif = dataSource
        detail.reloadInputViews()
        
        
        XCTAssertEqual(detail.titleLabel.text, "fat cat GIF")
        XCTAssertEqual(detail.ratingStar.rating, 0.0)
        
        // Test failing
//        let expectation = self.expectation(description: "Scaling")
//        if let url = URL(string: detail.gif!.image) {
//            detail.gifView.setGifFromURL(url)
//            expectation.fulfill()
//        }
//        waitForExpectations(timeout: 5, handler: nil)
//        XCTAssertNotNil(detail.gifView.image)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
