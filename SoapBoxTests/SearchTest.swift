//
//  SearchTest.swift
//  SoapBoxTests
//
//  Created by Hyewook Song on 2018-05-18.
//  Copyright © 2018 Hailey Song. All rights reserved.
//

import XCTest
@testable import SoapBox

class SearchTest: XCTestCase {
    
    var systemUnderTest: SearchViewController!
    var dataSource: [GIFModel] = []
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        systemUnderTest = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        _ = systemUnderTest.view
        
        dataSource = [GIFModel(id: "FG6EQNhs3s7ug",
                              url: "https://media2.giphy.com/media/FG6EQNhs3s7ug/200w.gif",
                              title: "fat cat GIF",
                              rating: 0.0)]
    }
    
    func test_CollectionView() {
        XCTAssertNotNil(systemUnderTest.collectionView)
    }
    
    func test_BarButtonItem() {
        XCTAssertNotNil(systemUnderTest.navigationItem.rightBarButtonItem)
        XCTAssertEqual(systemUnderTest.navigationItem.rightBarButtonItem?.title, "Sort")
    }
    
    func test_CellInCollectionView() {
        let collectionView = systemUnderTest.collectionView!
        systemUnderTest.dataArray = dataSource
        collectionView.reloadData()
        let cells = collectionView.visibleCells as! [GIFCell]
        
        XCTAssertNotNil(cells)
        
        for cell in cells {
            XCTAssertNotNil(cell.imageView.image)
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        self.dataSource = []
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
